<div align="center">
  <a href="https://gitlab.com/httpcompany/aquila" target="_blank"><img src="./assets/cover2x.png" height="150" alt="aquila-logo" /></a>
</div>

---

<div align="center">
  <p>An open-source full stack webapp with awesome features, built for academic purposes.</p>
</div>

<div align="center">
  <a href="#"><img src="https://badgen.net/badge/icon/gitlab?icon=gitlab&label"></a>
  <a href="#"><img src="https://img.shields.io/badge/Maintained%3F-yes-green.svg"></a>
  <a href="#"><img src="https://img.shields.io/badge/License-MIT-blue.svg"></a>
  <a href="#"><img src="https://img.shields.io/badge/code%20style-goodparts-brightgreen.svg?style=flat"></a>
  <a href="#"><img src="https://img.shields.io/badge/PRs-welcome-brightgreen.svg"></a>
</div>

## About

Aquila is an academic project which packs a lot of features together and built for National Higher Secondary School, Kerala. This is an open-source project developed by THC (The HTTP Company).

<!-- Few more words about the project -->

## Built with

  <!-- List of technologies used in the project -->
<div>
  <img src="https://img.icons8.com/color/128/000000/typescript.png" alt="typescript" width="50" height="50" />
  <img src="https://img.icons8.com/color/128/000000/react-native.png" alt="react" width="50" height="50" />
  <img src="https://img.icons8.com/color/128/000000/python.png" alt="python" width="50" height="50" />
  <img src="https://img.icons8.com/color/128/000000/django.png" alt="django" width="50" height="50" />
  <img src="https://img.icons8.com/color/128/000000/graphql.png" alt="graphql" width="50" height="50" />
  <img src="https://img.icons8.com/color/128/000000/docker.png" alt="docker" width="50" height="50" />
  <img src="https://img.icons8.com/color/128/000000/postgresql.png" alt="postgresql" width="50" height="50" />
  <img src="https://www.vectorlogo.zone/logos/nginx/nginx-icon.svg" alt="nginx" width="36" height="36" />
</div>

<br>

We use [GitLab](https://gitlab.com/httpcompany/aquila) as our primary source control system and If your looking this project from [Github](https://github.com/httpcompany/aquila) then probably it's a mirror of the GitLab instance.

## Features

- [x] **Material UI:** Minimal, clean and responsive design.

- [x] **SEO:** Rich User experiences in a uniform way, without compromising Search Engine Optimisation (SEO) factors that are key to good ranking on Google and other search engines.

- [x] **Gallery Page:** A gallery of images, videos and documents.

- [x] **Custom 404:** A custom 404 page for the website.

- [x] **Clean Code:** The code is clean and well documented.

If you have any feature requests or suggestions, please feel free to open an issue on [GitLab](https://gitlab.com/httpcompany/aquila).

## Contributing

To see how to Contribute, visit [DEVELOPERS.md](DEVELOPERS.md).

## License

This project is currently licensed under the **[MIT](LICENSE)**  